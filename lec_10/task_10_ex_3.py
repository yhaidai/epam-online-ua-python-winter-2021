"""
File `data/students.csv` stores information about students in CSV format.
This file contains the student’s names, age and average mark.

1. Implement a function get_top_performers which receives file path and
returns names of top performer students.
Example:
def get_top_performers(file_path, number_of_top_students=5):
    pass

print(get_top_performers("students.csv"))

Result:
['Teresa Jones', 'Richard Snider', 'Jessica Dubose', 'Heather Garcia',
'Joseph Head']

2. Implement a function write_students_age_desc which receives the file path
with students info and writes CSV student information to the new file in
descending order of age.
Example:
def write_students_age_desc(file_path, output_file):
    pass

Content of the resulting file:
student name,age,average mark
Verdell Crawford,30,8.86
Brenda Silva,30,7.53
...
Lindsey Cummings,18,6.88
Raymond Soileau,18,7.27
"""
import csv
from collections import namedtuple

Student = namedtuple('Student', 'name age gpa')


def get_top_performers(file_path, number_of_top_students=5):
    students = get_students(file_path)
    students.sort(key=lambda s: s.gpa, reverse=True)
    return [student.name for student in students[:number_of_top_students]]


def write_students_age_desc(file_path, output_file):
    students = get_students(file_path)
    students.sort(key=lambda s: s.age, reverse=True)
    
    with open(output_file, 'w') as f:
        f.write('student name,age,average mark\n')
        f.writelines(','.join(map(str, student)) + '\n' for student in students)


def get_students(file_path):
    with open(file_path, newline='') as csvfile:
        reader = csv.reader(csvfile)
        next(reader)  # skip titles
        return [
            Student(name, int(age), float(gpa)) for name, age, gpa in reader
        ]
