"""Implement a function `most_common_words(file_path: str, top_words: int) -> list`
which returns most common words in the file.
<file_path> - system path to the text file
<top_words> - number of most common words to be returned

Example:

print(most_common_words(file_path, 3))
>>> ['donec', 'etiam', 'aliquam']
> NOTE: Remember about dots, commas, capital letters etc.
"""
import re
from collections import Counter


def most_common_words(file_path, top_words):
    with open(file_path) as f:
        words = [word.lower() for word in re.findall(r'[a-zA-Z]+', f.read())]
    return Counter(words).most_common(top_words)
