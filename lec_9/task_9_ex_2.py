"""
Write a function that checks whether a string is a palindrome or not.
Return 'True' if it is a palindrome, else 'False'.

Note:
Usage of reversing functions is required.
Raise ValueError in case of wrong data type

To check your implementation you can use strings from here
(https://en.wikipedia.org/wiki/Palindrome#Famous_palindromes).
"""
import re


def is_palindrome(test_string: str) -> bool:
    if not isinstance(test_string, str):
        raise ValueError
    test_string = re.sub(r'[^a-zA-Z]', '', test_string).lower()
    return test_string == ''.join(reversed(test_string))


if __name__ == '__main__':
    print(is_palindrome('Able was I ere I saw Elba'))
