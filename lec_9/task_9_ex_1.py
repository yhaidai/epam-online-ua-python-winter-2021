"""
Task_9_1
Implement `swap_quotes` function which receives a string and replaces all " symbols with ' and vise versa.
The function should return modified string.

Note:
Usage of built-in or string replacing functions is required.
"""


def swap_quotes(some_string: str) -> str:
    return some_string.translate({ord('"'): "'", ord("'"): '"'})


if __name__ == '__main__':
    print(swap_quotes('Here\'s a single quote and that is a " double'))
