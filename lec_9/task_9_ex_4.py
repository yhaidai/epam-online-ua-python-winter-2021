"""
Implement a bunch of functions which receive a changeable number of strings and return next
parameters:
1) characters that appear in all strings
2) characters that appear in at least one string
3) characters that appear at least in two strings
  Note: raise ValueError if there are less than two strings
4) characters of alphabet, that were not used in any string
  Note: use `string.ascii_lowercase` for list of alphabet letters

Note: raise TypeError in case of wrong data type

Examples,
```python
test_strings = ["hello", "world", "python", ]
print(chars_in_all(*test_strings))
>>> {'o'}
print(chars_in_one(*test_strings))
>>> {'d', 'e', 'h', 'l', 'n', 'o', 'p', 'r', 't', 'w', 'y'}
print(chars_in_two(*test_strings))
>>> {'h', 'l', 'o'}
print(not_used_chars(*test_strings))
>>> {'q', 'k', 'g', 'f', 'j', 'u', 'a', 'c', 'x', 'm', 'v', 's', 'b', 'z', 'i'}
"""
import string
from collections import Counter


def chars_in_all(*strings):
    if not all(isinstance(s, str) for s in strings):
        raise TypeError
    return set.intersection(*[set(s) for s in strings])


def chars_in_one(*strings):
    if not all(isinstance(s, str) for s in strings):
        raise TypeError
    return set(''.join(strings))


def chars_in_two(*strings):
    if not all(isinstance(s, str) for s in strings):
        raise TypeError
    if len(strings) < 2:
        raise ValueError

    characters = ''.join([''.join(set(s)) for s in strings])
    return {k for k, v in Counter(characters).items() if v > 1}


def not_used_chars(*strings):
    if not all(isinstance(s, str) for s in strings):
        raise TypeError
    return set(string.ascii_lowercase) - set(''.join(strings))


if __name__ == '__main__':
    test_strings = ["hello", "world", "python", ]
    print(chars_in_all(*test_strings))
    print(chars_in_one(*test_strings))
    print(chars_in_two(*test_strings))
    print(not_used_chars(*test_strings))
