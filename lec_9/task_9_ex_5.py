"""
Implement function count_letters, which takes string as an argument and
returns a dictionary that contains letters of given string as keys and a
number of their occurrence as values.

Example:
print(count_letters("Hello world!"))
Result: {'H': 1, 'e': 1, 'l': 3, 'o': 2, 'w': 1, 'r': 1, 'd': 1}

Note: Pay attention to punctuation.
"""
from collections import Counter
import re


def count_letters(target: str):
    return Counter(re.sub(r'[^a-zA-z]', '', target))


if __name__ == '__main__':
    print(count_letters('Hello world!'))
