"""
Create CustomList – the linked list of values of random type, which size changes dynamically and has an ability to index
elements.

The task requires implementation of the following functionality:
• Create the empty user list and the one based on enumeration of values separated by commas. The elements are stored
in form of unidirectional linked list. Nodes of this list must be implemented in class Item.
    Method name: __init__(self, *data) -> None;
• Add and remove elements.
    Method names: append(self, value) -> None - to add to the end,
                add_start(self, value) -> None - to add to the start,
                remove(self, value) -> None - to remove the first occurrence of given value;
• Operations with elements by index. Negative indexing is not necessary.
    Method names: __getitem__(self, index) -> Any,
                __setitem__(self, index, data) -> None,
                __delitem__(self, index) -> None;
• Receive index of predetermined value.
    Method name: find(self, value) -> Any;
• Clear the list.
    Method name: clear(self) -> None;
• Receive lists length.
    Method name: __len__(self) -> int;
• Make CustomList iterable to use in for-loops;
    Method name: __iter__(self);
• Raise exceptions when:
    find() or remove() don't find specific value
    index out of bound at methods __getitem__, __setitem__, __delitem__.


Notes:
    The class CustomList must not inherit from anything (except from the object, of course).
    Function names should be as described above. Additional functionality has no naming requirements.
    Indexation starts with 0.
    List length changes while adding and removing elements.
    Inside the list the elements are connected as in a linked list, starting with link to head.
"""
from typing import Any


class Item:
    """
    A node in a unidirectional linked list.
    """
    def __init__(self, data, next_=None):
        self.data = data
        self.next = next_

    def __str__(self):
        return str(self.data)


class CustomList:
    """
    An unidirectional linked list.
    """
    def __init__(self, *data) -> None:
        self.length = len(data)
        self.head = Item(data[0]) if data else None

        current = self.head
        for value in data[1:]:
            current.next = Item(value)
            current = current.next

    def __getitem__(self, index) -> Any:
        if index >= len(self):
            raise IndexError

        current = self.head
        for _ in range(index):
            current = current.next

        return current.data

    def __setitem__(self, index, data) -> None:
        if index >= len(self):
            raise IndexError

        current = self.head
        for _ in range(index):
            current = current.next

        current.data = data

    def __delitem__(self, index) -> None:
        if index >= len(self):
            raise IndexError

        prev, current = None, self.head
        for _ in range(index):
            prev, current = current, current.next

        if prev is not None:
            prev.next = current.next
        else:
            self.head = current.next
        del current
        self.length -= 1

    def __len__(self) -> int:
        return self.length

    def __iter__(self):
        return CustomListIterator(self)

    def __str__(self):
        return ', '.join(str(i) for i in self)

    def append(self, value) -> None:
        """Add element to the end of the list"""
        item = None
        for item in self:
            pass

        if item is None:
            self.head = Item(value)
        else:
            item.next = Item(value)
        self.length += 1

    def add_start(self, value) -> None:
        """Add element to the start of the list"""
        old_head = self.head
        self.head = Item(value, old_head)
        self.length += 1

    def remove(self, value) -> None:
        """Remove the first occurrence of given value in the list"""
        prev, current = None, self.head
        while current:
            if current.data == value:
                if prev is not None:
                    prev.next = current.next
                else:
                    self.head = current.next

                del current
                self.length -= 1
                return None

            prev, current = current, current.next

        raise ValueError

    def find(self, value) -> Any:
        """Receive index of predetermined value."""
        index = 0
        for item in self:
            if item.data == value:
                return index
            index += 1

        raise ValueError

    def clear(self) -> None:
        """Clear the list."""
        for item in self:
            del item

        self.length = 0
        self.head = None


class CustomListIterator:
    def __init__(self, custom_list: CustomList):
        self.current = custom_list.head

    def __iter__(self):
        return self

    def __next__(self):
        if self.current is None:
            raise StopIteration

        prev, self.current = self.current, self.current.next
        return prev


if __name__ == '__main__':
    lst = CustomList(*range(10))
    print(lst)
    lst.remove(5)
    print(lst)
    lst[1] = 20
    print(lst)
    lst.add_start(33)
    print(lst)
    print(lst.find(20))
