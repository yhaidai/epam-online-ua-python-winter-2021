"""
Implement function combine_dicts, which receives a changeable
number of dictionaries (keys - letters, values - integers)
and combines them into one dictionary.

Dict values should be summarized in case of identical keys.

Example:
dict_1 = {'a': 100, 'b': 200}
dict_2 = {'a': 200, 'c': 300}
dict_3 = {'a': 300, 'd': 100}

combine_dicts(dict_1, dict_2)
Result: {'a': 300, 'b': 200, 'c': 300}

combine_dicts(dict_1, dict_2, dict_3)
Result: {'a': 600, 'b': 200, 'c': 300, 'd': 100}
"""
from string import ascii_letters


def combine_dicts(*dicts):
    result = {}
    for d in dicts:
        for key, value in d.items():
            if key not in list(ascii_letters):
                raise KeyError
            if not isinstance(value, int):
                raise ValueError

            result[key] = result.get(key, 0) + value

    return result


if __name__ == '__main__':
    print(combine_dicts(
        {'a': 100, 'b': 200},
        {'a': 200, 'c': 300},
        {'a': 300, 'd': 100},
    ))
