"""
Implement a function `split_by_index(string: str, indexes: List[int]) -> List[str]`
which splits the `string` by indexes specified in `indexes`.
Only positive index, larger than previous in list is considered valid.
Invalid indexes must be ignored.

Examples:
```python
>>> split_by_index("pythoniscool,isn'tit?", [6, 8, 12, 13, 18])
["python", "is", "cool", ",", "isn't", "it?"]

>>> split_by_index("pythoniscool,isn'tit?", [6, 8, 8, -4, 0, "u", 12, 13, 18])
["python", "is", "cool", ",", "isn't", "it?"]

>>> split_by_index("no luck", [42])
["no luck"]
```
"""


def split_by_index(string, indexes):
    result = []
    left = 0
    indexes.append(len(string))
    for right in indexes:
        if not isinstance(right, int) or left >= right:
            continue

        result.append(string[left:right])
        left = right

    return result


if __name__ == '__main__':
    print(split_by_index(
        "pythoniscool,isn'tit?", [6, 8, 8, -4, 0, "u", 12, 13, 24]
    ))
