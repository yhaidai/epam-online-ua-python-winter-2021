"""
Task04_1_2
Write `is_palindrome` function that checks whether a string is a palindrome or not
Returns 'True' if it is palindrome, else 'False'

To check your implementation you can use strings from here
(https://en.wikipedia.org/wiki/Palindrome#Famous_palindromes).

Note:
Usage of any reversing functions is prohibited.
The function has to ignore special characters, whitespaces and different cases
Raise ValueError in case of wrong data type
"""


def is_palindrome(test_string: str) -> bool:
    if not isinstance(test_string, str):
        raise ValueError

    target = ''.join(filter(lambda c: c.isalpha(), test_string)).lower()

    reversed_ = []
    for char in target:
        reversed_.insert(0, char)
    reversed_ = ''.join(reversed_)

    return target == reversed_


if __name__ == '__main__':
    print(is_palindrome('AAb1v !baa'))
