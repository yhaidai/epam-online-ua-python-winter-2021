"""
For a positive integer n calculate the result value, which is equal to the sum
of the odd numbers of n.

Example,
n = 1234 result = 4
n = 246 result = 0

Write it as function.

Note:
Raise TypeError in case of wrong data type or negative integer;
Use of 'functools' module is prohibited, you just need simple for loop.
"""


def sum_odd_numbers(n: int) -> int:
    if type(n) is not int or n <= 0:
        raise TypeError
    return sum((x if (x := int(d)) % 2 == 1 else 0 for d in str(n)))


if __name__ == '__main__':
    print(sum_odd_numbers(246))
