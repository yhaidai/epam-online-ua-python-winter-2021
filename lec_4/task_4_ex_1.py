"""04 Task 1.1
Implement a function which receives a string and replaces all " symbols with ' and vise versa. The
function should return modified string.
Usage of any replacing string functions is prohibited.
"""


def swap_quotes(string: str) -> str:
    quotes = {
        '"': "'",
        "'": '"',
    }
    return ''.join([quotes.get(char, char) for char in string])


if __name__ == '__main__':
    print(swap_quotes('This \' is a single quote and this is a double "'))
