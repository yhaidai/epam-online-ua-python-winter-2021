"""
Task04_3

Implement a function which works the same as str.split

Note:
Usage of str.split method is prohibited
Raise ValueError in case of wrong data type
"""
import re


def split_alternative(str_to_split: str, delimiter: str = None) -> list:
    if not isinstance(str_to_split, str):
        raise ValueError

    result = []
    if delimiter is None:
        delimiter = r'\s+'

    while match := re.search(delimiter, str_to_split):
        result.append(str_to_split[0:match.start()])
        str_to_split = str_to_split[match.end():]
    result.append(str_to_split)

    return result


if __name__ == '__main__':
    print(split_alternative('Tue, 12 April, 2000'))
