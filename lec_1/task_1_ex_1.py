"""01-Task1-Task1
Write a Python-script that performs simple arithmetic operations: '+', '-', '*', '/'. The type of operator and
data are set on the command line when the script is run.
The script should be launched like this:
$ python my_task.py 1 * 2

Notes:
For other operations need to raise NotImplementedError.
Do not dynamically execute your code (for example, using exec()).
Use the argparse module to parse command line arguments. Your implementation shouldn't require entering any
parameters (like -f or --function).
"""
import argparse


def calculate(args):
    operators = {
        '+': lambda x, y: x + y,
        '-': lambda x, y: x - y,
        '*': lambda x, y: x * y,
        '/': lambda x, y: x / y,
    }

    if args.operator not in operators:
        raise NotImplementedError

    return operators[args.operator](
        float(args.operand_1), float(args.operand_2)
    )


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('operand_1')
    parser.add_argument('operator')
    parser.add_argument('operand_2')
    args = parser.parse_args()
    print(calculate(args))


if __name__ == '__main__':
    main()
