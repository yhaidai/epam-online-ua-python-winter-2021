"""01-Task1-Task2
Write a Python-script that performs the standard math functions on the data. The name of function and data are
set on the command line when the script is run.
The script should be launched like this:
$ python my_task.py add 1 2

Notes:
Function names must match the standard mathematical, logical and comparison functions from the built-in libraries.
The script must raises all happened exceptions.
For non-mathematical function need to raise NotImplementedError.
Use the argparse module to parse command line arguments. Your implementation shouldn't require entering any
parameters (like -f or --function).
"""
import argparse
import math
import operator


def calculate(args):
    built_in_math_functions = {
        f: getattr(m, f) for m in [operator, math] for f in dir(m)
    }

    if args.function in built_in_math_functions:
        return built_in_math_functions[args.function](*args.arguments)
    else:
        raise NotImplementedError


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('function')
    parser.add_argument('arguments', nargs='+', type=float)
    args = parser.parse_args()
    print(calculate(args))


if __name__ == '__main__':
    main()
