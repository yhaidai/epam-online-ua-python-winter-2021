"""
Write a function converting a Roman numeral from a given string N into an
Arabic numeral. Values may range from 1 to 100 and may contain invalid
symbols. Invalid symbols and numerals out of range should raise ValueError.

Numeral / Value:
I: 1
V: 5
X: 10
L: 50
C: 100

Example:
N = 'I'; result = 1
N = 'XIV'; result = 14
N = 'LXIV'; result = 64

Example of how the task should be called:
python3 task_3_ex_2.py LXIV

Note: use `argparse` module to parse passed CLI arguments
"""
import argparse

arabic = {
    'I': 1,
    'V': 5,
    'X': 10,
    'L': 50,
    'C': 100,
}


def from_roman_numerals(roman):
    if len(roman) <= 1:
        return arabic[roman]

    return (from_roman_numerals(roman[:-1])
            + arabic[roman[-1]]
            - (arabic[roman[-2]] < arabic[roman[-1]] and 2 * arabic[roman[-2]]))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('roman_numerals')
    args = parser.parse_args()

    try:
        print(from_roman_numerals(args.roman_numerals))
    except KeyError as e:
        raise ValueError from e


if __name__ == "__main__":
    main()
